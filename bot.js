const Discord = require('discord.js')
const axios = require('axios')
require('dotenv').config()

const client = new Discord.Client()

let cards = []

client.on('ready', () => {
    console.log('ready')
    axios.get('https://api.hearthstonejson.com/v1/25770/enUS/cards.json')
    .then(res => {
        cards = res.data
    })
})

client.on('message', async message => {
    if(message.content.includes('[')) {
        let cardName = await message.content.substring(
            message.content.lastIndexOf('[') + 1,
            message.content.lastIndexOf(']')
        )

        await console.log(cardName)

        let card = await findCardByName(cardName)
        let embededCard = await new Discord.RichEmbed()
            .setTitle(card.name)
            .setDescription(card.text)
            .addField("Name")

        await message.channel.send(embededCard)
    }
})

const findCardByName = cardName => {
    for(let i=0; i<cards.length; i++) {
        let name = cards[i].name
        if(name.toLowerCase() === cardName) {
            return cards[i]
        }
    }
}

client.login(process.env.TOKEN)